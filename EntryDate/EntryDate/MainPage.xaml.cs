﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EntryDate
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.btnSave.IsEnabled = false;
        }

        //async void해도 await하면 창이 닫힐때까지 기다린다.
        private async void BtnSave_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("날짜", "저장 완료", "OK");
            Debug.WriteLine("========================================Wait 할까?");
        }
    }
}
