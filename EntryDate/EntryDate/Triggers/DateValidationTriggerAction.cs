﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace EntryDate.Triggers
{
    public class DateValidationTriggerAction : TriggerAction<Entry>
    {
        protected override void Invoke(Entry entry)
        {
            Regex LongDate = new Regex(@"[0-9]{4}-[0-9]{2}-[0-9]{2}");
            DateTime result;
            bool isValid = false;

            if (LongDate.Match(entry.Text.Trim()).Success)
            {
                Debug.WriteLine("Date Regex Success");
                isValid = DateTime.TryParse(entry.Text.Trim(), out result);
                if (isValid)
                {
                    Debug.WriteLine("Date DateValidation Success");
                    Debug.WriteLine(result.ToString());
                }
                else
                {
                    Debug.WriteLine("Date DateValidation Fail");
                }
            }
            else
            {
                Debug.WriteLine("Date Regex Fail");
            }

            
            entry.TextColor = isValid ? Color.Default : Color.Red;
        }
    }
}
